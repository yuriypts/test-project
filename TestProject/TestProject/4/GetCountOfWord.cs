﻿using System;
using TestProject._2;
using TestProject._3;

namespace TestProject._4
{
    public static class GetCountOfWord
    {
        public static int GetCountOfWordFromFiles(this string path, string word)
        {
            var newValue = 0;
            if (String.IsNullOrEmpty(word) || String.IsNullOrEmpty(path))
            {
                return newValue;
            }

            var files = path.ListOfAllFiles();

            if (files.Count < 1)
            {
                return newValue;
            }

            IStringOperations operations = new StringOperations();

            for (var i = 0; i < files.Count; i++)
            {
                newValue = newValue + operations.GetCountOfWord(files[i], word);
            }

            return newValue;
        }
    }
}
