﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestProject._2;
using TestProject._3;
using TestProject._4;
using TestProject._5;

namespace TestProject
{
    class Program
    {
        public void asd()
        {
            "asd".ListOfAllFiles(); ;
        }

        static void Main(string[] args)
        {
            var list = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };
            var listIsSorted = IsAscendentSortedList(list);

            Console.WriteLine("1 -> List is Sorted? - {0}", listIsSorted);

            IStringOperations operations = new StringOperations();

            Console.WriteLine("2 -> Inserts additional strings right into the beginning and into the end of the input string  - {0}", operations.AddSubstring("helllo", "    asd"));
            Console.WriteLine("2 -> Gets Number of appearances of word in input string  - {0}", operations.GetCountOfWord("helllo asd", " asd"));
            Console.WriteLine("2 -> Removes special characters from input string  - {0}", operations.RemoveSpecialCharacters("helllo asd 2]][222 asd as w", "[0-9]"));

            var pathDirectory = "D://test";
            var listFiles = pathDirectory.ListOfAllFiles();
            for (var i = 0; i < listFiles.Count; i++)
            {
                Console.WriteLine("3 -> File names from directory and all subdiretories - {0}", listFiles[i]);
            }

            Console.WriteLine("4 -> Get count of specified word from all files from specified path from - {0}", pathDirectory.GetCountOfWordFromFiles("asd"));

            GenerateRandomData randomData = new GenerateRandomData();

            foreach (var item in randomData.People)
            {
                Console.WriteLine("5 -> FirstName - {0}, LastName - {1}, YearOfBirth- {2}, PersonSex - {3}", item.FirstName, item.LastName, item.YearOfBirth, item.PersonSex);
            }

            Console.WriteLine("6 -> Count of Peters - {0} and Count of all McLeans - {1}", randomData.People.Count(x => x.FirstName == "Peter"), randomData.People.Count(x => x.LastName == "McLean"));
            Console.WriteLine("6 -> Males - {0 } /Females - {1} /Unknown - {2}", randomData.People.Count(x => x.PersonSex == "male"), randomData.People.Count(x => x.PersonSex == "female"), randomData.People.Count(x => x.PersonSex == "unknown"));
            Console.WriteLine("6 -> The average age of all persons - {0}", randomData.People.Max(x => x.YearOfBirth));
            Console.WriteLine("6 -> The total age of all persons born after - {0}", randomData.People.Count(x => x.YearOfBirth > new DateTime(1960, 1, 1)));
            Console.WriteLine("6 -> How many male Jonh Schlanskys were born before 2000 - {0}", randomData.People.Count(x => x.FirstName == "Jonh" && x.LastName == "Schlansky" && x.YearOfBirth < new DateTime(2000, 1, 1)));

            Console.ReadKey();
        }

        static bool IsAscendentSortedList(List<int> list)
        {
            var isAscSortList = true;

            for (var i = 0; i < list.Count; i++)
            {
                var minValue = list.Min(x => x);
                var firstValue = list.First();

                if (firstValue != minValue)
                {
                    isAscSortList = false;
                    break;
                }

                list.Remove(firstValue);
            }

            return isAscSortList;
        }
    }
}
