﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestProject
{
    public interface IStringOperations
    {
        /// <summary>
        /// Inserts additional strings right into the beginning and into the end of the 
        /// input string
        /// </summary>
        /// <param name="inputString">Target string</param>
        /// <param name="additionalString">Additional string</param>
        /// <returns>Resulting string</returns>
        string AddSubstring(string inputString, string additionalString);

        /// <summary>
        /// Gets Number of appearances of word in input string 
        /// </summary>
        /// <param name="inputString">String to search characters for</param>
        /// <param name="word">Search word</param>
        /// <returns>Count of appearances of specified word</returns>
        int GetCountOfWord(string inputString, string word);

        /// <summary>
        /// Removes special characters from input string
        /// </summary>
        /// <param name="inputString">Target string to remove special characters</param>
        /// <param name="regex">String "array" of special characters</param>
        /// <returns></returns>
        string RemoveSpecialCharacters(string inputString, string regex);
    }
}
