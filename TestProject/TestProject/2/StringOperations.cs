﻿using System;
using System.Text.RegularExpressions;

namespace TestProject._2
{
    public class StringOperations : IStringOperations
    {
        public string AddSubstring(string inputString, string additionalString)
        {
            if (String.IsNullOrEmpty(inputString) || String.IsNullOrEmpty(additionalString))
            {
                return String.Empty;
            }

            return additionalString.Trim() + inputString + additionalString.Trim();
        }

        public int GetCountOfWord(string inputString, string word)
        {
            var countWordsInInputString = 0;
            if (String.IsNullOrEmpty(inputString) || String.IsNullOrEmpty(word))
            {
                return countWordsInInputString;
            }

            for (var i = 0; i < word.Length; i++)
            {
                if (inputString.IndexOf(word[i]) != -1)
                {
                    countWordsInInputString = countWordsInInputString + 1;
                }
            }
            

            return countWordsInInputString;
        }

        public string RemoveSpecialCharacters(string inputString, string regex)
        {
            var newValue = String.Empty;
            if (String.IsNullOrEmpty(inputString) || String.IsNullOrEmpty(regex))
            {
                return newValue;
            }

            newValue = Regex.Replace(inputString, regex, "");
            return newValue;
        }
    }
}
