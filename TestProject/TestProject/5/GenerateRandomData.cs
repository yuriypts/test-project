﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestProject._5
{
    public class GenerateRandomData
    {
        string[] FirstNames = { "Helen", "Alicia", "Mary", "John", "Peter", "Martin" };
        string[] LastNames = { "Collins", "O'Neal", "McLean", "Schlansky", "Murphy", "Hawtorn", "Jordan", "Gahan" };
        string[] PersonSex = { "male", "female", "unknown" };

        public List<Person> People = new List<Person>();

        public GenerateRandomData()
        {
            Random rand = new Random();

            for (var i = 0; i < FirstNames.Length; i++)
            {
                var indexLastName = rand.Next(FirstNames.Length);

                var model = new Person
                {
                    FirstName = FirstNames[i],
                    LastName = LastNames[indexLastName],
                    YearOfBirth = GetYearsOfBirth(rand),
                    PersonSex = PersonSex[rand.Next(0, PersonSex.Length)]
                };

                People.Add(model);
            }
        }

        DateTime GetYearsOfBirth(Random rand)
        {
            DateTime start = new DateTime(1960, 1, 1);
            return new DateTime(rand.Next(start.Year, DateTime.Today.Year), 1, 1);
        }
    }
}
