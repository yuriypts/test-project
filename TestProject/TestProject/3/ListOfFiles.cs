﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestProject._3
{
    public static class ListOfFiles
    {
        public static List<string> ListOfAllFiles(this string path)
        {
            var isExistDirectory = Directory.Exists(path);
            var lsit = new List<string>();

            if (isExistDirectory)
            {
                var files = Directory.GetFiles(path, "*", SearchOption.AllDirectories);

                for (var i = 0; i < files.Length; i++)
                {
                    lsit.Add(new FileInfo(files[i]).Name);
                }
            }

            return lsit.Distinct().ToList();
        }
    }
}
